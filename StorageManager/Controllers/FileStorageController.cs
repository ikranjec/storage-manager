﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StorageManager.Helpers;
using StorageManager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorageManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileStorageController : ControllerBase
    {
        private FileUploadService _fileUploadService;

        public FileStorageController()
        {
            _fileUploadService = new FileUploadService();
        }

        [HttpPost(nameof(Upload))]
        public ValidationResultHelper Upload()
        {
            var result = _fileUploadService.Upload(HttpContext);
            return result.Result;
        }
    }
}
