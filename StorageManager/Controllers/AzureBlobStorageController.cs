﻿using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Storage.Net;
using Storage.Net.Blobs;
using StorageManager.Interfaces;
using StorageManager.Requests;
using StorageManager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace StorageManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AzureBlobStorageController : ControllerBase
    {
        private IBlobService _blobService;
        private IConfiguration _configuration;
        private string _containerName;

        public AzureBlobStorageController(IBlobService blobService, IConfiguration configuration)
        {
            _blobService = blobService;
            _configuration = configuration;
            _containerName = _configuration.GetSection("AzureBlobStorage:DefaultContainerName").Value;
        }

        [HttpGet(nameof(GetBlob))]
        public async Task<IActionResult> GetBlob(string blobName)
        {
            var data = await _blobService.GetBlobAsync(_containerName, blobName);
            return File(data.Content, data.ContentType);
        }

        [HttpGet(nameof(GetBlobs))]
        public async Task<IActionResult> GetBlobs()
        {
            return Ok(await _blobService.ListBlobsAsync(_containerName));
        }

        [HttpPost(nameof(UploadFile))]
        public async Task<IActionResult> UploadFile([FromBody] UploadFileRequest request)
        {
            await _blobService.UploadFileBlobAsync(request.FilePath, request.FileName, _containerName);
            return Ok();
        }

        [HttpPost(nameof(UploadContent))]
        public async Task<IActionResult> UploadContent([FromBody]UploadContentRequest request)
        {
            await _blobService.UploadContentBlobAsync(request.Content, request.FileName, _containerName);
            return Ok();
        }

        
        [HttpDelete(nameof(DeleteFile))]
        public async Task<IActionResult> DeleteFile(string blobName)
        {
            await _blobService.DeleteBlobAsync(blobName, _containerName);
            return Ok();
        }
    }
}
