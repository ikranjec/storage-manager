﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace StorageManager.Helpers
{
    public class BlobInfoHelper
    {
        public Stream Content { get; set; }
        public string ContentType { get; set; }

        public BlobInfoHelper(Stream content, string contentType)
        {
            Content = content;
            ContentType = contentType;
        }
    }
}
