﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorageManager.Helpers
{
    public class ValidationResultHelper
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }

        public ValidationResultHelper(int statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }
    }
}
