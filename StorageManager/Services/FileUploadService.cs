﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;
using StorageManager.Constants;
using StorageManager.Helpers;
using System.IO;
using System.Threading.Tasks;

namespace StorageManager.Services
{
    public class FileUploadService
    {
        public HttpRequest Request { get; set; }
        public MultipartReader Reader { get; set; }
        public MultipartSection Section { get; set; }
        public bool HasContentDispositionHeader { get; set; }

        public FileUploadService()
        {

        }

        public async Task<ValidationResultHelper> Upload(HttpContext httpContext)
        {
            Request = httpContext.Request;

            if (!Request.HasFormContentType || !MediaTypeHeaderValue.TryParse(Request.ContentType, out var mediaTypeHeader) || string.IsNullOrEmpty(mediaTypeHeader.Boundary.Value))
            {
                return new ValidationResultHelper(
                    ValidationResultConstants.UNSUPPORTED_MEDIA_TYPE_RESULT_CODE,
                    ValidationResultConstants.UNSUPPORTED_MEDIA_TYPE_RESULT_MESSAGE
                );
            }

            Reader = new MultipartReader(mediaTypeHeader.Boundary.Value, Request.Body);
            Section = await Reader.ReadNextSectionAsync();

            while (Section != null)
            {
                HasContentDispositionHeader = ContentDispositionHeaderValue.TryParse(Section.ContentDisposition, out var contentDisposition);

                if (HasContentDispositionHeader && contentDisposition.DispositionType.Equals("form-data") && !string.IsNullOrEmpty(contentDisposition.FileName.Value))
                {
                    var fileName = contentDisposition.FileName.Value;
                    var saveToPath = Path.Combine(Path.GetTempPath(), fileName);

                    using (var targetStream = System.IO.File.Create(saveToPath))
                    {
                        await Section.Body.CopyToAsync(targetStream);
                    }

                    return new ValidationResultHelper(
                        ValidationResultConstants.OK_RESULT_CODE,
                        ValidationResultConstants.OK_RESULT_MESSAGE
                    );
                }

                Section = await Reader.ReadNextSectionAsync();
            }

            return new ValidationResultHelper(
                ValidationResultConstants.BAD_REQUEST_CODE,
                ValidationResultConstants.BAD_REQUEST_RESULT_MESSAGE
            );
        }
    }
}
