﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorageManager.Constants
{
    public static class ValidationResultConstants
    {
        public const string UNSUPPORTED_MEDIA_TYPE_RESULT_MESSAGE = "Unsuppoerted MediaType Result!";
        public const int UNSUPPORTED_MEDIA_TYPE_RESULT_CODE = 415;

        public const string OK_RESULT_MESSAGE = "Ok!";
        public const int OK_RESULT_CODE = 200;

        public const string BAD_REQUEST_RESULT_MESSAGE = "Bad Request!";
        public const int BAD_REQUEST_CODE = 400;
    }
}
