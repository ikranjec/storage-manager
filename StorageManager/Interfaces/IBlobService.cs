﻿using StorageManager.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorageManager.Interfaces
{
    public interface IBlobService
    {
        public Task<BlobInfoHelper> GetBlobAsync(string containerName, string blobName);
        public Task<IEnumerable<string>> ListBlobsAsync(string containerName);
        public Task UploadFileBlobAsync(string filePath, string fileName, string containerName);
        public Task UploadContentBlobAsync(string content, string fileName, string containerName);
        public Task DeleteBlobAsync(string blobName, string containerName);
    }
}
